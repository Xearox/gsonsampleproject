package de.xearox.sample;

import java.io.IOException;

import de.xearox.sample.config.ConfigHandler;
import de.xearox.sample.config.UserConfig;

public class GsonSampleProject {

	public static void main(String[] args) {
		ConfigHandler handler = new ConfigHandler();
		
		/*
		 * Create the Config File
		 */
		try {
			handler.createConfigFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * Loads the Config File from external directory
		 */
		try {
			UserConfig config = handler.loadConfigFile();
			System.out.println("Config in external directory");
			System.out.println("TextSize : " + config.getTextSize());
			System.out.println("Start Again? : " + config.isStartAgain());
			System.out.println("Centered Text? : " + config.isCenterText());
			System.out.println("#################################################");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * Loads the Config File from internal resource directory
		 */
		try {
			UserConfig config = handler.loadConfigInJar();
			System.out.println("Config in internal resource directory");
			System.out.println("TextSize : " + config.getTextSize());
			System.out.println("Start Again? : " + config.isStartAgain());
			System.out.println("Centered Text? : " + config.isCenterText());
			System.out.println("#################################################");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
