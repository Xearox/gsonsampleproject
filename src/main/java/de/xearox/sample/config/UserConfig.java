package de.xearox.sample.config;

public class UserConfig {

	private boolean startAgain;
	private boolean centerText;
	private int textSize;
	
	public UserConfig() {
		// TODO Auto-generated constructor stub
	}
	
	public UserConfig(boolean startAgain, boolean centerText, int textSize) {
		this.startAgain = startAgain;
		this.centerText = centerText;
		this.textSize = textSize;
	}

	public boolean isStartAgain() {
		return startAgain;
	}

	public void setStartAgain(boolean startAgain) {
		this.startAgain = startAgain;
	}

	public boolean isCenterText() {
		return centerText;
	}

	public void setCenterText(boolean centerText) {
		this.centerText = centerText;
	}

	public int getTextSize() {
		return textSize;
	}

	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}
	
	

}
