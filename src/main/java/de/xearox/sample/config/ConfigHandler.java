package de.xearox.sample.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import de.xearox.sample.GsonSampleProject;

public class ConfigHandler {

	private static ConfigHandler instance;

	public static ConfigHandler instance() {
		return ConfigHandler.instance;
	}

	public void createConfigFile() throws IOException {
		String path = getApplicationPath();

		File configFile = new File(path + File.separator + "userConfig.json");

		if (configFile.exists())
			return;

		UserConfig config = new UserConfig(true, false, 10);

		Writer writer = new FileWriter(configFile);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		gson.toJson(config, writer);
		writer.flush();
	}

	public UserConfig loadConfigFile() throws IOException {
		String path = getApplicationPath();

		File configFile = new File(path + File.separator + "userConfig.json");

		if (!configFile.exists())
			return null;

		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new FileReader(configFile));

		UserConfig config = gson.fromJson(reader, UserConfig.class);

		return config;
	}

	public UserConfig loadConfigInJar() throws IOException {
		
		String path = getResourcePath("userConfig.json");
		
		if(path == null) {
			System.out.println("PATH == NULL");
			return null;
		}
		
		InputStream is = new FileInputStream(new File(path));

		InputStreamReader isr = new InputStreamReader(is);

		JsonReader reader = new JsonReader(isr);

		Gson gson = new Gson();
		UserConfig config = gson.fromJson(reader, UserConfig.class);

		return config;
	}

	public static String getApplicationPath() {
		try {
			return new File(".").getCanonicalPath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public static String getResourcePath(String relativePath){
	    String pathStr = null;
	    URL location = GsonSampleProject.class.getProtectionDomain().getCodeSource().getLocation();
	    String codeLocation = location.toString();
	    try{
	        if (codeLocation.endsWith(".jar")){
	            //Call from jar
	            Path path = Paths.get(location.toURI()).resolve("../classes/" + relativePath).normalize();
	            pathStr = path.toString();
	        }else{
	            //Call from IDE
	            pathStr = GsonSampleProject.class.getClassLoader().getResource(relativePath).getPath();
	        }
	    }catch(URISyntaxException ex){
	        ex.printStackTrace();
	    }
	    return pathStr;
	}

}
